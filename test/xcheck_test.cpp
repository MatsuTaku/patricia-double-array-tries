#include <dicpdat/da_util.hpp>
#include <vector>
#include <set>
#include <iostream>
#include <memory>
#include <cmath>

std::vector<int> fb(std::vector<uint8_t> lables, std::vector<int> map, std::set<int> ban_base) {
  std::vector<int> res;
  for (int i = 0; i < 256; i++) if (ban_base.count(i) == 0) {
    bool ok = true;
    for (auto c : lables) ok &= map[i ^ c] == 0;
    if (ok) res.push_back(i);
  }
  return res;
}

template<typename T>
void showvec(std::vector<T> vec) {
  for (auto v:vec) std::cout<<v<<",";
  std::cout<<std::endl;
}

int main() {
  std::vector<std::vector<uint8_t>> labels = {
      {144,241,124,198},
      {179,33,94,42,189,190,196,90,167,212,241},
      {79,139,218,45,43,163,203,107,19,98,245,121,67},
      {205,219,78,9,43},
      {200,164,168,167,134,112,247,72,245},
      {145},
      {4,40,61,120,126,249,128,137,144,110,191,231,250,172,77},
      {89,27,218,201,210,101,131,232,25,42,63,203,137,67,50},
      {10,31,184,210,208,95,167,117},
      {17,11,57,54,52,102,104,207,62},
      {231,96,57,254,215,234,49,76,103,13},
      {238,175,154},
      {40},
      {20,154,180,},
      {52,74,246,167,119,39,112,90,},
      {84,158,251,8,237,162,241,118,244,33,},
      {30,66,54,69,192,226,56,},
  };
  std::vector<int> expected_base_list = {0,1,2,5,8,3,15,212,43,46,67,4,10,7,119,79,72};

  uint64_t map_mask[4] = {};
  memset(map_mask, ~0, sizeof(uint64_t)*4);
  uint64_t base_mask[4] = {};
  memset(base_mask, ~0, sizeof(uint64_t)*4);
  std::vector<int> result;
  int k = 0;
  for (auto& l : labels) {
    auto base = dicpdat::da_util::xcheck_in_da_block(map_mask, l, base_mask);

    if (base != expected_base_list[k]) {
      std::cerr << "base != expected_base_list[k]" << std::endl;
      assert(false);
      exit(EXIT_FAILURE);
    }
    ++k;
    if ((base_mask[base/64] & (1ull<<(base%64))) == 0) {
      std::cerr << "(base_mask[base/64] & (1ull<<(base%64))) == 0" << std::endl;
      assert(false);
      exit(EXIT_FAILURE);
    }
    base_mask[base/64] &= ~(1ull<<(base%64));
    for (auto c : l) {
      auto id = c^base;
      if ((map_mask[id/64] & (1ull<<(id%64))) == 0) {
        std::cerr << "(map_mask[id/64] & (1ull<<(id%64))) == 0" << std::endl;
        assert(false);
        exit(EXIT_FAILURE);
      }
      map_mask[id/64] &= ~(1ull<<(id%64));
    }
    result.push_back(base);
  }
  for (int i = 0; i < 4; i++) std::cout<<std::bitset<64>(map_mask[i])<<std::endl;

}