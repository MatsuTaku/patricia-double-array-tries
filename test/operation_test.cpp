#include <dicpdat.hpp>

namespace {
constexpr int alphabet_size = 3;
}

std::vector<std::string> make_keyset() {
  std::vector<std::string> ret;
  std::string s;
  for (int i = 0; i < alphabet_size; i++) {
    s += std::string(2, 'a'+i);
    for (int j = 0; j < alphabet_size; j++) {
      s += std::string(2, 'a'+j);
      for (int k = 0; k < alphabet_size; k++) {
        s += std::string(2, 'a'+k);
        ret.push_back(s);
        s.resize(s.size()-2);
      }
      s.resize(s.size()-2);
    }
    s.resize(s.size()-2);
  }
  for (size_t i = 0; i < ret.size(); i++) {
    std::swap(ret[i], ret[rand()%ret.size()]);
  }
  return ret;
}

template<typename T>
void test_dict(const std::vector<std::string>& keyset) {
  size_t n = keyset.size();
  T dict;
  for (size_t i = 0; i < n; i++) {
    auto ret = dict.insert(keyset[i]);
    assert(ret.second);
    *(ret.first) = i;
    for (size_t j = 0; j <= i; j++) {
      auto p = dict.find(keyset[j]);
      assert(p and *p == j);
    }
  }
}

template<typename T>
void test_dict2(const std::vector<std::string>& keyset) {
  size_t n = keyset.size();
  T dict;
  for (size_t i = 0; i < n; i++) {
    dict.insert(keyset[i], i);
    for (size_t j = 0; j <= i; j++) {
      auto p = dict.find(keyset[j]);
      assert(p and *p == j);
    }
  }
}

int main() {
  auto keyset = make_keyset();
  test_dict<dicpdat::DoubleArrayMpTrie<int, uint32_t, false, false, 1, true>>(keyset);
//  test_dict<dicpdat::DoubleArrayPatriciaTrie<int, uint32_t, false, false, 1, true>>(keyset);
//  test_dict2<dicpdat::DoubleArrayDpdTrie<int>>(keyset);
}
