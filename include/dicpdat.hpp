#ifndef DICPDAT_HPP_
#define DICPDAT_HPP_

#include "dicpdat/DoubleArrayPatriciaTrie.hpp"
//#include "dicpdat/DoubleArrayMpTrie.hpp"
//#include "dicpdat/DoubleArrayDpdTrie.hpp"

namespace dicpdat {

using string_set32 = DoubleArrayPatriciaTrieInterface<void, uint32_t, false, true, 1, false>;

using string_set64 = DoubleArrayPatriciaTrieInterface<void, uint64_t, false, true, 1, false>;

using string_set = std::conditional_t<sizeof(size_t) == 8,
                                      string_set64,
                                      string_set32>;

template <typename ValueType>
using string_map32 = DoubleArrayPatriciaTrieInterface<ValueType, uint32_t, false, true, 1, false>;

template <typename ValueType>
using string_map64 = DoubleArrayPatriciaTrieInterface<ValueType, uint64_t, false, true, 1, false>;

template <typename ValueType>
using string_map = std::conditional_t<sizeof(size_t) == 8,
                                      string_map64<ValueType>,
                                      string_map32<ValueType>>;

using unordered_string_set32 = DoubleArrayPatriciaTrieInterface<void, uint32_t, false, false, 1, false>;

using unordered_string_set64 = DoubleArrayPatriciaTrieInterface<void, uint64_t, false, false, 1, false>;

using unordered_string_set = std::conditional_t<sizeof(size_t) == 8,
                                                unordered_string_set64,
                                                unordered_string_set32>;

template <typename ValueType>
using unordered_string_map32 = DoubleArrayPatriciaTrieInterface<ValueType, uint32_t, false, false, 1, false>;

template <typename ValueType>
using unordered_string_map64 = DoubleArrayPatriciaTrieInterface<ValueType, uint64_t, false, false, 1, false>;

template <typename ValueType>
using unordered_string_map = std::conditional_t<sizeof(size_t) == 8,
                                                unordered_string_map64<ValueType>,
                                                unordered_string_map32<ValueType>>;

}

#endif //DICPDAT_HPP_
