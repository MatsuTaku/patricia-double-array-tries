#ifndef DOUBLEARRAYDPDTRIE_HPP_
#define DOUBLEARRAYDPDTRIE_HPP_

#include "internal/DoubleArrayTrieDictImpl.hpp"
#include "DoubleArrayPatriciaTrie.hpp"
#include <cstring>
#include <string_view>
#include <utility>
#include <tuple>

namespace dicpdat {

template<typename TransLabelType, typename CharType, unsigned CharWidth>
class _DoubleArrayDpdTrieDefinition {
 public:
  static constexpr unsigned kCharWidth = CharWidth;

  static TransLabelType make_trans_label(uint8_t fork_pos, CharType c) {
    return ((TransLabelType)fork_pos << kCharWidth) | c;
  }
  static std::tuple<uint8_t, CharType> get_trans_tuple(TransLabelType trans) {
    uint8_t fork_pos = trans >> kCharWidth;
    CharType c = trans % (1ull<<kCharWidth);
    return {fork_pos, c};
  }

};

/* Fix for
 * - ordered:      disabled
 * - max_trial:    1
 * - legacy_build: true
 */
template<typename T, typename DaImpl, bool BitOperationalFind>
class _DoubleArrayDpdTrieConstructor
 : public _DynamicDoubleArrayPatriciaTrieConstructor<DaImpl, T, typename DaImpl::_index_type,
 false, // letter check
 false, // ordered
 1,     // max trial
 BitOperationalFind> // bit-operational build
{
  using _super = _DynamicDoubleArrayPatriciaTrieConstructor<DaImpl, T, typename DaImpl::_index_type, false, false, 1, BitOperationalFind>;
 public:
  using _index_type = typename DaImpl::_index_type;
  using _trans_label_type = typename DaImpl::_trans_label_type;
  using char_type = uint8_t;
  using definition = _DoubleArrayDpdTrieDefinition<_trans_label_type, uint8_t, 8>;
  using mapped_pointer = typename DaImpl::mapped_pointer;

  static constexpr _index_type kRootIndex = DaImpl::kRootIndex;

  explicit _DoubleArrayDpdTrieConstructor(DaImpl& da_impl) : _super(da_impl) {}
  virtual ~_DoubleArrayDpdTrieConstructor() = default;
  
  using _super::da_;

 public:
  // Insert new suffix and get container-index of pointer to storing value.
  mapped_pointer insert(_index_type node, size_t label_offset, size_t matched_len, const char* cptr, size_t len) {
    if (matched_len < len) {
      auto trans_label = definition::make_trans_label(label_offset, cptr[matched_len]);
      auto new_node = _super::_InsertTrans(node, trans_label);
      auto base_index = da_.AppendTarget(DaImpl::_unit_reference::kEmptyFlag);
      da_.AppendLabelInPool(matched_len + 1 < len ? std::string_view(cptr + matched_len + 1) : "");
      auto map_pointer = da_.AppendEmptyValue();
      da_.unit_at(new_node).set_pool_index(base_index, false);
      return reinterpret_cast<mapped_pointer>(map_pointer);
    } else { // New node must be leaf forever.
      auto trans_label = definition::make_trans_label(label_offset, '\0');
      auto new_node = _super::_InsertTrans(node, trans_label);
      da_.unit_at(new_node).set_pool_index(da_.pool_size(), true);
      return da_.AppendEmptyValue();
    }
  }
};

/* Fix for
 * - index_bits:   32bit
 * - trans_label_bits: 16bit(max_label_length = 255, alphabet_size = 256) // TODO
 */
template<typename T, bool BitOperationalFind>
 class _DoubleArrayDpdTrie : public internal::_DoubleArrayTrieDictImpl<T,
    uint32_t, // index type
    uint16_t , // trans label type
    1ull<<16, // trans label size
    false>    // letter check
{
 public:
  using mapped_type = T;
  using mapped_pointer = mapped_type*;
  using const_mapped_pointer = const mapped_type*;

  static constexpr size_t kTransLabelSize = 4;
  static constexpr bool kLetterCheck = false;

  using _super = internal::_DoubleArrayTrieDictImpl<T, uint32_t, uint16_t, 1ull<<16, kLetterCheck>;
  using _index_type = typename _super::_index_type;
  using _trans_label_type = typename _super::_trans_label_type;

  using char_type = uint8_t;
  static constexpr char_type kEndChar = '\0';
  static constexpr _index_type kTransFailure = ~(_index_type)(0);
  static constexpr _index_type kRootIndex = 0;
  static constexpr _trans_label_type kEmptyLabel = _super::kEmptyLabel;
  static constexpr _trans_label_type kLeafLabel [[maybe_unused]] = 0;

  using constructor = _DoubleArrayDpdTrieConstructor<mapped_type, _DoubleArrayDpdTrie, BitOperationalFind>;
  using definition = _DoubleArrayDpdTrieDefinition<_index_type, char_type, 8>;

 private:
  constructor constructor_;

 public:
  _DoubleArrayDpdTrie() : constructor_(*this) {
    constructor_.init();
  }

  size_t size_in_bytes() const override {
    return _super::size_in_bytes();
  }

  std::pair<mapped_pointer, bool> insert(const char* cptr, size_t len, const T& value) {
    auto [ptr, created] = _create_mapped_space(cptr, len);
    if (created)
      *ptr = value;
    return {ptr, created};
  }

  const_mapped_pointer find(const char* cptr, size_t len) const {
    return _traverse(cptr, len, [](const_mapped_pointer ptr) {
      return ptr;
    }, [](auto...){
      return nullptr;
    }).first;
  }
  mapped_pointer find(const char* cptr, size_t len) {
    return _traverse(cptr, len, [](mapped_pointer ptr) {
      return ptr;
    }, [](auto...){
      return nullptr;
    }).first;
  }

  template <class Action>
  void for_each_children(_index_type node, Action action) const {
    assert(not _super::unit_at(node).is_leaf());
    auto base = _super::base_at(node);
    _trans_label_type child;
    if constexpr (kLetterCheck) {
      child = _super::unit_at(base).child();
    } else {
      child = _super::unit_at(node).child();
    }
    std::vector<int> c;
    while (child != kEmptyLabel) {
      auto next = base xor child;
      c.push_back(next);
      assert(not _super::unit_empty_at(next));
      assert(_super::unit_at(next).check() == (kLetterCheck ? child : node));
      auto sibling = _super::unit_at(next).sibling();
      action(next, child);
      child = sibling;
    }
  }

  bool is_leaf_at(_index_type node) const override {
    return _super::unit_at(node).base_empty() or _super::base_at(node) == _super::_unit_reference::_common::kEmptyFlag;
  }

 private:
  _index_type _op(_index_type base, _trans_label_type label) const {
    return base ^ label;
  }
  _index_type _iop(_index_type node, _trans_label_type label) const {
    return node ^ label;
  }
  _index_type _go_to(_index_type node, _index_type trans_label) const {
    if (is_leaf_at(node)) {
      return kTransFailure;
    }
    auto next_node = _op(_super::base_at(node), trans_label);
    auto nu = _super::unit_at(next_node);
    if (!nu.check_empty() and nu.check() == node) {
      return next_node;
    } else {
      return kTransFailure;
    }
  }
  // [label_ptr, match_failed_offset, matched_len, exact_match_result]
  template<typename Succeed, typename Failed>
  std::pair<const_mapped_pointer, bool>
  _traverse(const char* cptr, size_t len, Succeed success, Failed failure) const {
    auto node = _go_to(kRootIndex, definition::make_trans_label(0, len != 0 ? cptr[0] : kEndChar));
    if (node == kTransFailure) {
      return {failure(kRootIndex, nullptr, 0, 0), false};
    } else if (len == 0) {
      return {success(reinterpret_cast<const_mapped_pointer>(_super::pool_ptr_at(_super::unit_at(node).pool_index()))), true};
    }
    auto lptr = _super::pool_ptr_at(_super::unit_at(node).pool_index()+_super::kIndexSize);
    size_t offset = 0;
    size_t p = 1;
    while (p < len) {
      while (p+offset < len and (char_type)*(lptr+offset) == (char_type)(cptr[p+offset])) ++offset;
      if (p+offset == len) {
        break;
      }
      auto next_node = _go_to(node, definition::make_trans_label(offset, cptr[p+offset]));
      if (next_node == kTransFailure)
        return {failure(node, lptr, offset, p + offset), false};
      p += offset+1;
      offset = 0;
      node = next_node;
      lptr = _super::pool_ptr_at(_super::unit_at(node).pool_index()+_super::kIndexSize);
    }
    // Check transition by leaf_char=kEndChar
    if ((char_type)*(lptr+offset) == kEndChar) {
      return std::make_pair(success(reinterpret_cast<const_mapped_pointer>(lptr+offset+1)), true); // Exact matched
    } else {
      auto next_node = _go_to(node, definition::make_trans_label(offset, kEndChar));
      if (next_node != kTransFailure)
        return std::make_pair(success(reinterpret_cast<const_mapped_pointer>(_super::pool_ptr_at(_super::unit_at(next_node).pool_index()))), true);
      else
        return std::make_pair(failure(node, lptr, offset, len), false);
    }
  }

  template<typename Succeed, typename Failed>
  std::pair<mapped_pointer, bool>
  _traverse(const char* cptr, size_t len, Succeed success, Failed failure) {
    auto node = _go_to(kRootIndex, definition::make_trans_label(0, len != 0 ? cptr[0] : kEndChar));
    if (node == kTransFailure) {
      return {failure(kRootIndex, nullptr, 0, 0), false};
    } else if (len == 0) {
      return {success(reinterpret_cast<mapped_pointer>(_super::pool_ptr_at(_super::unit_at(node).pool_index()))), true};
    }
    auto lptr = _super::pool_ptr_at(_super::unit_at(node).pool_index()+_super::kIndexSize);
    size_t offset = 0;
    size_t p = 1;
    while (p < len) {
      while (p+offset < len and (char_type)*(lptr+offset) == (char_type)(cptr[p+offset])) ++offset;
      if (p+offset == len) {
        break;
      }
      auto next_node = _go_to(node, definition::make_trans_label(offset, cptr[p+offset]));
      if (next_node == kTransFailure)
        return {failure(node, lptr, offset, p + offset), false};
      p += offset+1;
      offset = 0;
      node = next_node;
      lptr = _super::pool_ptr_at(_super::unit_at(node).pool_index()+_super::kIndexSize);
    }
    // Check transition by leaf_char=kEndChar
    if ((char_type)*(lptr+offset) == kEndChar) {
      return std::make_pair(success(reinterpret_cast<mapped_pointer>(lptr+offset+1)), true); // Exact matched
    } else {
      auto next_node = _go_to(node, definition::make_trans_label(offset, kEndChar));
      if (next_node != kTransFailure)
        return std::make_pair(success(reinterpret_cast<mapped_pointer>(_super::pool_ptr_at(_super::unit_at(next_node).pool_index()))), true);
      else
        return std::make_pair(failure(node, lptr, offset, len), false);
    }
  }

  std::pair<mapped_pointer, bool> _create_mapped_space(const char* cptr, size_t len) {
    auto ret = _traverse(cptr, len, [](auto ptr) { return ptr; },
                                   [&](_index_type node, auto, size_t loffset, size_t matched_len) {
      return constructor_.insert(node, loffset, matched_len, cptr, len);
    });
    ret.second = !ret.second;
    return ret;
  }

};

template<typename T, bool BitOperationalFind = false>
class DoubleArrayDpdTrie : private _DoubleArrayDpdTrie<T, BitOperationalFind> {
  using _super = _DoubleArrayDpdTrie<T, BitOperationalFind>;
 public:
  using key_type = std::string;
  using typename _super::mapped_type, typename _super::mapped_pointer, typename _super::const_mapped_pointer;
  using value_type = std::pair<key_type, mapped_type>;

  DoubleArrayDpdTrie() = default;
  virtual ~DoubleArrayDpdTrie() = default;

  template<typename It>
  DoubleArrayDpdTrie(It begin, It end) : DoubleArrayDpdTrie() {
    for (auto it = begin; it != end; ++it) insert(it->first, it->second);
  }

  size_t size_in_bytes() const {
    return _super::size_in_bytes();
  }

  std::pair<mapped_pointer, bool> insert(const char* cptr, const T& value) {
    return _super::insert(cptr, strlen(cptr), value);
  }
  std::pair<mapped_pointer, bool> insert(const std::string& str, const T& value) {
    return _super::insert(str.data(), str.length(), value);
  }
  std::pair<mapped_pointer, bool> insert(std::string_view sv, const T& value) {
    return _super::insert(sv.data(), sv.length(), value);
  }

  const_mapped_pointer find(const char* cptr, size_t len) const {
    return _super::find(cptr, len);
  }
  const_mapped_pointer find(const char* cptr) const {
    return _super::find(cptr, strlen(cptr));
  }
  const_mapped_pointer find(const std::string& str) const {
    return _super::find(str.data(), str.length());
  }
  const_mapped_pointer find(std::string_view sv) const {
    return _super::find(sv.data(), sv.length());
  }

  mapped_pointer find(const char* cptr, size_t len) {
    return _super::find(cptr, len);
  }
  mapped_pointer find(const char* cptr) {
    return _super::find(cptr, strlen(cptr));
  }
  mapped_pointer find(const std::string& str) {
    return _super::find(str.data(), str.length());
  }
  mapped_pointer find(std::string_view sv) {
    return _super::find(sv.data(), sv.length());
  }

};

} // namespace dicpdat

#endif // DOUBLEARRAYDPDTRIE_HPP_