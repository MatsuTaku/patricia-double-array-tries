//
// Created by 松本拓真 on 2019/12/28.
//

#ifndef DICPDAT_INCLUDE_DICPDAT_IOUTIL_HPP_
#define DICPDAT_INCLUDE_DICPDAT_IOUTIL_HPP_

#include <iostream>
#include <vector>

namespace dicpdat::ioutil {

// MARK: Read

template<typename T>
inline T read_val(std::istream& is) {
  T val;
  is.read(reinterpret_cast<char*>(&val), sizeof(val));
  return val;
}

template<typename T, class A>
inline void read_vec(std::istream& is, std::vector<T, A>& vec) {
  auto size = read_val<size_t>(is);
  vec.resize(size);
  is.read(reinterpret_cast<char*>(vec.data()), sizeof(T) * size);
}

inline std::string read_string(std::istream& is) {
  auto size = read_val<size_t>(is);
  std::string str(size, 0);
  is.read(reinterpret_cast<char*>(&str[0]), sizeof(char) * size);
  return str; // expect move
}

// MARK: Write

template<typename T>
inline void write_val(const T& val, std::ostream& os) {
  os.write(reinterpret_cast<const char*>(&val), sizeof(val));
}

template<typename T, class A>
inline void write_vec(const std::vector<T, A> &vec, std::ostream &os) {
  write_val(vec.size(), os);
  os.write(reinterpret_cast<const char*>(&vec[0]), sizeof(T) * vec.size());
}

inline void write_string(const std::string &str, std::ostream &os) {
  write_val(str.size(), os);
  os.write(reinterpret_cast<const char*>(&str[0]), sizeof(char) * str.size());
}

template<typename T, class A>
inline size_t size_vec(const std::vector<T, A>& vec) {
  return sizeof(T) * vec.size() + sizeof(vec.size());
}

}

#endif //DICPDAT_INCLUDE_DICPDAT_IOUTIL_HPP_
