//
// Created by 松本拓真 on 2019-08-10.
//

#ifndef DOUBLEARRAYTRIEDICTIMPL_HPP
#define DOUBLEARRAYTRIEDICTIMPL_HPP

#include "DoubleArrayContainer.hpp"

#include "ioutil.hpp"

namespace dicpdat::internal {


template <typename ValueType, typename IndexType>
class _DoubleArrayTrieDictConstant {
 public:
  static constexpr size_t kValueSize = sizeof(ValueType);
  static constexpr size_t kIndexSize = sizeof(IndexType);
};

template <typename IndexType>
class _DoubleArrayTrieDictConstant<void, IndexType> {
 public:
  static constexpr size_t kValueSize = 0;
  static constexpr size_t kIndexSize = sizeof(IndexType);
};


template <typename ValueType, typename IndexType, typename TransLabelType, unsigned TransLabelSize, bool LetterCheck>
 class _DoubleArrayTrieDictImpl : private _DoubleArrayTrieDictConstant<ValueType, IndexType>, public internal::_DoubleArrayContainer<IndexType, TransLabelType, TransLabelSize, LetterCheck> {
 public:
  using _constant = _DoubleArrayTrieDictConstant<ValueType, IndexType>;
  using _base = internal::_DoubleArrayContainer<IndexType, TransLabelType, TransLabelSize, LetterCheck>;
  using _value_type = ValueType;
  using _value_pointer = _value_type*;
  using _const_value_pointer = const _value_type*;
  using _index_type = typename _base::_index_type;
  using _trans_label_type = typename _base::_trans_label_type;
  using _storage_type = uint8_t;

  static constexpr size_t kValueSize = _constant::kValueSize;
  static constexpr size_t kIndexSize = _constant::kIndexSize;

  static constexpr _trans_label_type kEmptyLabel = ~(_trans_label_type)(0);
  using char_type = uint8_t;
  static constexpr char_type kEndChar = 0;

  using typename _base::Index2;

 private:
  std::vector<_storage_type> label_pool_;

 public:
  _DoubleArrayTrieDictImpl() : _base() {}

  // MARK: Basic information

  size_t size_in_bytes() const override {
    return _base::size_in_bytes() + ioutil::size_vec(label_pool_);
  }

  size_t bc_size_in_bytes() const {
    return _base::kUnitSize * _base::unit_size();
  }

  size_t bc_blank_size_in_bytes() const {
    return bc_size_in_bytes() - _base::kUnitSize * _base::num_nodes();
  }

  size_t pool_size_in_bytes() const {return label_pool_.size();}

  size_t pool_blank_size_in_bytes() const {
    size_t blank_size = label_pool_.size();
    for (size_t i = 0; i < _base::unit_size(); i++) {
      auto unit = _base::unit_at(i);
      if (unit_empty_at(i) or not unit.has_label())
        continue;
      if (unit.label_is_suffix()) {
        blank_size -= label_in_pool(unit.pool_index()).size() + kValueSize;
        if (not unit.has_label())
            blank_size--;
      } else {
        blank_size -= kIndexSize + label_in_pool(unit.pool_index() + kIndexSize).size();
      }
    }
    return blank_size;
  }

  size_t succinct_size_in_bytes() const {
    return size_in_bytes() - bc_size_in_bytes() - pool_size();
  }

  // Load factor of double-array.
  float load_factor_alpha() const {
    return _base::load_factor();
  }

  // Load factor of label pool.
  float load_factor_beta() const {
    return float(pool_blank_size_in_bytes()) / label_pool_.size();
  }

  float load_factor() const override {
    return float(bc_size_in_bytes() + pool_size_in_bytes() - bc_blank_size_in_bytes() - pool_blank_size_in_bytes()) / float(bc_size_in_bytes() + pool_size_in_bytes());
  }

  // MARK: Double-array Trie dict behavior

  bool unit_empty_at(size_t index) const {
    Index2 id(index);
    return _base::block_at(id.block_index).empty_element_at(id.unit_insets);
  }

  virtual bool is_leaf_at(_index_type node) const {
    return _base::unit_at(node).is_leaf();
  }

  _index_type base_at(_index_type index) const {
      auto unit = _base::unit_at(index);
      return not unit.has_label() ? unit.base() : target_in_pool_at(unit.pool_index());
  }

  void set_base_at(_index_type index, _index_type new_value) {
      auto unit = _base::unit_at(index);
      if (not unit.has_label() or unit.label_is_suffix()) {
          unit.set_base(new_value);
      } else {
          set_target_in_pool_at(unit.pool_index(), new_value);
      }
  }

  size_t pool_size() const {return label_pool_.size();}

  const _storage_type* pool_ptr_at(size_t index) const {return label_pool_.data() + index;}
  _storage_type * pool_ptr_at(size_t index) {return label_pool_.data() + index;}

  _storage_type char_in_pool_at(size_t index) const {return label_pool_[index];}

  void set_char_in_pool_at(size_t index, _trans_label_type new_value) {
    label_pool_[index] = new_value;
  }

  std::string_view label_in_pool(size_t index) const {
    return std::string_view((const char*)label_pool_.data() + index);
  }

  size_t AppendLabelInPool(std::string_view label) {
    auto index = label_pool_.size();
    label_pool_.resize(label_pool_.size() + label.size() + 1);
    for (size_t i = 0; i < label.size(); i++)
      label_pool_[index+i] = label[i];
    label_pool_[index+label.size()] = kEndChar;
    return index;
  }

  _const_value_pointer value_ptr_in_pool_at(size_t index) const {
    return reinterpret_cast<_const_value_pointer>(label_pool_.data() + index);
  }
  _value_pointer value_ptr_in_pool_at(size_t index) {
    return reinterpret_cast<_value_pointer>(label_pool_.data() + index);
  }

  _value_pointer AppendEmptyValue() {
    auto index = label_pool_.size();
    label_pool_.resize(label_pool_.size() + kValueSize);
    return value_ptr_in_pool_at(index);
  }

  size_t target_in_pool_at(size_t index) const {
    return *reinterpret_cast<const _index_type*>(label_pool_.data() + index);
  }

  void set_target_in_pool_at(size_t index, _index_type new_value) {
    *reinterpret_cast<_index_type*>(label_pool_.data() + index) = new_value;
  }

  size_t AppendTarget(_index_type value) {
    auto index = label_pool_.size();
    label_pool_.resize(label_pool_.size() + kIndexSize);
    set_target_in_pool_at(index, value);
    return index;
  }

};

}

#endif //DOUBLEARRAYTRIEDICTIMPL_HPP
