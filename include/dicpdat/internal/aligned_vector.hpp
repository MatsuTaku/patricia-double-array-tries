#ifndef DICPDAT_ALIGNED_VECTOR_HPP_
#define DICPDAT_ALIGNED_VECTOR_HPP_

#include "boost/align/aligned_allocator.hpp"
#include <vector>

namespace dicpdat {

template <typename T, unsigned Alignment>
using aligned_vector = std::vector<T, boost::alignment::aligned_allocator<T, Alignment>>;

}

#endif //DICPDAT_ALIGNED_VECTOR_HPP_
