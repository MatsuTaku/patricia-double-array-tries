#include "dicpdat.hpp"
#include "dicpdat/DoubleArrayDpdTrie.hpp"

#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <chrono>

namespace {

class Stopwatch {
  using hrc = std::chrono::high_resolution_clock;
  hrc::time_point start_;
 public:
  Stopwatch() : start_(hrc::now()) {}
  auto time_process() const {
    return hrc::now() - start_;
  }
  double get_sec() const {
    return std::chrono::duration<double>(time_process()).count();
  }
  double get_milli_sec() const {
    return std::chrono::duration<double, std::milli>(time_process()).count();
  }
  double get_micro_sec() const {
    return std::chrono::duration<double, std::micro>(time_process()).count();
  }
};

template <class Process>
double milli_sec_in(Process process) {
  Stopwatch sw;
  process();
  return sw.get_milli_sec();
}

std::vector<std::string> make_sample_keyset(size_t size) {
  std::vector<std::string> keyset;
  const size_t kMaxKeySize = 32;
  const size_t kCharPattern = 'z'-'a'+1;
  for (size_t i = 0; i < size; i++) {
    std::string s;
    auto key_size = rand() % kMaxKeySize+1;
    for (size_t j = 0; j < key_size; j++) {
      s.push_back('a'+uint8_t(rand() % kCharPattern));
    }
    keyset.push_back(s);
  }
  sort(keyset.begin(), keyset.end());
  keyset.erase(std::unique(keyset.begin(), keyset.end()), keyset.end());
  return keyset;
}

std::vector<std::string> large_keyset = make_sample_keyset(0x100000);


void SetSample() {
  std::cout << "Test set with small sample" << std::endl;
  std::vector<std::string> set = {
      "ab",
      "abc",
      "b",
      "bac",
      "bb"
  };
  using double_array_type = dicpdat::string_set;
  double_array_type da(set);
  for (std::string_view s : set) {
    if (not da.contains(s)) {
      std::cerr << "Failed to find " << s << std::endl;
      return;
    }
  }
  std::cout << "  OK" << std::endl;
}

void SetSampleLarge() {
  std::cout << "Test set with large sample" << std::endl;
  using double_array_type = dicpdat::string_set;
  Stopwatch sw;
  double_array_type da(large_keyset);
  std::cout << "Build time(m): " << sw.get_milli_sec() << std::endl;
  std::cout << "Memory size(B): " << da.size_in_bytes() << std::endl;
  auto t = sw.get_micro_sec();
  for (std::string_view s : large_keyset) {
    if (not da.contains(s)) {
      std::cerr << "Failed to find " << s << std::endl;
      return;
    }
  }
  std::cout << "Lookup time(µs): " << (sw.get_micro_sec()-t)/large_keyset.size() << std::endl;
  std::cout << "  OK" << std::endl;
}

void MapSample() {
  std::cout << "Test map with small sample" << std::endl;
  std::vector<std::string> set = {
      "ab",
      "abc",
      "b",
      "bac",
      "bb"
  };
  using double_array_type = dicpdat::string_map<char>;
  double_array_type da(set);
  for (std::string_view s : set) {
    if (da.find(s) == nullptr) {
      std::cerr << "Failed to find " << s << std::endl;
      return;
    }
  }
  std::cout << "  OK" << std::endl;
}

void MapSampleLarge() {
  std::cout << "Test map with large sample" << std::endl;
  using double_array_type = dicpdat::string_map<char>;
  Stopwatch sw;
  double_array_type da(large_keyset);
  std::cout << "Build time(m): " << sw.get_milli_sec() << std::endl;
  std::cout << "Memory size(B): " << da.size_in_bytes() << std::endl;
  auto t = sw.get_micro_sec();
  for (std::string_view s : large_keyset) {
    auto ptr = const_cast<const decltype(da)&>(da).find(s);
    if (!ptr) {
      std::cerr << "Failed to find " << s << std::endl;
      return;
    }
  }
  std::cout << "Lookup time(µs): " << (sw.get_micro_sec()-t)/large_keyset.size() << std::endl;
  std::cout << "  OK" << std::endl;
}

void MapSampleDpd() {
  std::cout << "Test dpd-map with small sample" << std::endl;
  std::vector<std::string> set = {
      "ab",
      "abc",
      "b",
      "bac",
      "bb"
  };
  using double_array_type = dicpdat::DoubleArrayDpdTrie<int>;
  double_array_type da;
  int id = 0;
  for (auto& l : set)
    da.insert(l, id++);
  id = 0;
  for (std::string_view s : set) {
    auto ptr = const_cast<const decltype(da)&>(da).find(s);
    if (!ptr or *ptr != id) {
      std::cerr << "Failed to find " << s << std::endl;
      return;
    }
    id++;
  }
  std::cout << "  OK" << std::endl;
}

void MapSampleDpdLarge() {
  std::cout << "Test dpd-map with large sample" << std::endl;
  using double_array_type = dicpdat::DoubleArrayDpdTrie<int>;
  Stopwatch sw;
  double_array_type da;
  int id = 0;
  for (auto& l : large_keyset) da.insert(l, id++);
  std::cout << "Build time(m): " << sw.get_milli_sec() << std::endl;
  std::cout << "Memory size(B): " << da.size_in_bytes() << std::endl;
  id = 0;
  auto t = sw.get_micro_sec();
  for (std::string_view s : large_keyset) {
    auto ptr = const_cast<const decltype(da)&>(da).find(s);
    if (!ptr or *ptr != id) {
      std::cerr << "Failed to find " << s << std::endl;
      return;
    }
    id++;
  }
  std::cout << "Lookup time(µs): " << (sw.get_micro_sec()-t)/large_keyset.size() << std::endl;
  std::cout << "  OK" << std::endl;
}

}

int main() {
  SetSample();
  SetSampleLarge();
  MapSample();
  MapSampleLarge();

  MapSampleDpd();
  MapSampleDpdLarge();

  return 0;
}
