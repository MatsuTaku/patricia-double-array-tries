# patricia-double-array-tries

Dynamic string dictionary supporting fast lookup implemented by Patricia Double-Array Tries.

This repository is implementation of following article:

T. Matsumoto, K. Morita, M. Fuketa. Implementation of Dynamic Keyword Dictionary by Patricia Trie Using Double-Array(Japanese paper). TOD, Vol. 85, No. 13-2, p34-44. 
https://ipsj.ixsq.nii.ac.jp/ej/?action=pages_view_main&active_action=repository_view_main_item_detail&item_id=204374&item_no=1&page_id=13&block_id=8

## Data structures
 
### String set
Data structure `dicpdat::string_set` stores a set of strings and provides following operations:
 - insert(*key*): Insert string *key*.
 - erase(*key*): Erase string *key*.
 - contains(*key*): Weather string *key* is contained.
 
### String map
Data structure `dicpdat::string_map | dicpdat::unordered_string_map` stores some values with string key, and provides following operations:
 - insert(*key*, *value*): Insert string *key* with *value*.
 - erase(*key*): Erase string *key* and corresponding *value*.
 - find(*key*): If string *key* is contained, return pointer of corresponding *value*. Otherwise return `nullptr`.

## Dependencies
 - Boost: required to install your environment.
 - libbo: included this repository as submodule.
